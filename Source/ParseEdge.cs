﻿using System;
using System.Collections.Generic;
using Models;
using System.Linq;

namespace Source
{
    public class ParseEdge
    {
        private readonly string _text;
        private const string _elementText = "link";
        private readonly ParseFormatter _parser;

        public ParseEdge(string text)
        {
            _text = text ?? throw new ArgumentNullException(nameof(text));
            _parser = new ParseFormatter(text, _elementText);
        }

        public Edge GetLink()
        {
            if (! _text.Contains(@"/link"))
                throw new ArgumentException("Line does not contain attribute /link");

            return FormatAttributes();
        }

        private Edge FormatAttributes()
        {
            List<KeyValuePair<string, string>> linkAttributes = _parser.GetAttributes();
            ValidateAttributes(linkAttributes);

            return new Edge()
            {
                Id = linkAttributes.Single(attribute => attribute.Key == "id").Value,
                Nodes = linkAttributes.
                    Where(attribute => attribute.Key == "node").
                    Select(attribute => attribute.Value).
                    ToArray(),
                Length = double.Parse(linkAttributes.Single(attribute => attribute.Key == "length").Value)
            };
        }

        private static void ValidateAttributes(List<KeyValuePair<string,string>> attributes)
        {
            List<string> attributeKeys = attributes.Select(attribute => attribute.Key).ToList();
            if (attributeKeys.Count(key => key == "id") > 1)
                throw new ArgumentException("Invalid number of id attributes");

            if (attributeKeys.Count(key => key == "node") > 2)
                throw new ArgumentException("Invalid number of node attributes");

            if (attributeKeys.Count(key => key == "length") > 1)
                throw new ArgumentException("Invalid number of length attributes");

        }        
    }
}
