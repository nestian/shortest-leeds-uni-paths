﻿using System;
using System.Collections.Generic;
using System.Linq;
using Models;

//@author Sali Hyusein

namespace Source
{
    //A class that can find the shortest path
    //between two nodes (if exists) in a given
    //Graph with nodes and edges
    public class PathFinder
    {
        private readonly List<string> _nodes;
        private readonly List<Edge> _edges;
        private readonly Dictionary<string,List<KeyValuePair<string,double>>> adjSets;
        private Dictionary<string,double> _nodeWeights;
        private Dictionary<string, bool> _traversed;
        private Dictionary<string, string> _previous;
        private List<string> _onTraverse; 
        //to backtrace path from destination to source
        private Stack<string> _pathStack = new Stack<string>();
        private double _pathLength;

        public PathFinder(List<string> nodes, List<Edge> edges)
        {
            _nodes = nodes ?? throw new ArgumentNullException("Null list passed to pathfinder");
            _edges = edges ?? throw new ArgumentNullException("Null list passed to pathfinder");

            adjSets = new AdjacentSets(_nodes, _edges).GetAll();
        }

        //@returns the shortest existing path between two nodes
        //null otherwise
        //
        //Implementation of Dijkstra's algorithm for shortest path
        public List<string> GetShortestPath(string source, string destination)
        {
            if (source == null || destination == null) throw new ArgumentNullException("Null source or destination");

            InitDictionaries();
            source = GetNode(source, _nodes);
            destination = GetNode(destination, _nodes);
            _nodeWeights[source] = 0; //to start iterating from source

            //foreach vertex in the Graph
            while (_onTraverse.Any())
            {
                string current = GetNodeWithLeastWeight();
                if (current == null)
                    break;

                List<KeyValuePair<string,double>> ofCurrent = adjSets[current];
                if (ofCurrent != null)
                {
                    //check distances to each neighbouring vertex to reallocate weight
                    foreach(var item in ofCurrent)
                    {
                        double thisDistance = item.Value + _nodeWeights[current];
                        if(thisDistance < _nodeWeights[item.Key] && _traversed[item.Key] == false)
                        {
                            _nodeWeights[item.Key] = thisDistance;
                            _previous[item.Key] = current;
                        }
                    }
                }

                _traversed[current] = true; //mark the node to not iterate on it again
                _onTraverse.Remove(current);
            }

            if (_nodeWeights[destination] >= double.MaxValue) return null; //no path exists from source

            _pathLength = 0;
            BuildPath(source, destination);
            return ExtractStack();
        }

        private void InitDictionaries()
        {
            _nodeWeights = new Dictionary<string, double>();
            _traversed = new Dictionary<string, bool>();
            _previous = new Dictionary<string, string>();
            _onTraverse = new List<string>();

            foreach(string node in _nodes)
            {
                _nodeWeights.Add(node, double.MaxValue);
                _traversed.Add(node, false);
                _previous.Add(node, null);
                _onTraverse.Add(node);
            }
        }

        //builds a path by backtracing shortest route to
        //destination until the source is reached
        private void BuildPath(string source, string destination)
        {
            _pathStack.Push(destination);
            _pathLength += _nodeWeights[destination];
            //path is complete
            if(_previous[destination] == source)
            {
                _pathStack.Push(source);
                _pathLength += _nodeWeights[source];
            }

            //recursion until the source node is reached
            else if (_previous[destination] != source)
            {
                BuildPath(source, _previous[destination]);
            }

        }

        private List<string> ExtractStack()
        {
            List<string> path = new List<string>();

            while(_pathStack.Any())
            {
                path.Add(_pathStack.Pop());
            }

            return path;
        }

        public double PathLength()
        {
            return _pathLength;
        }

        private string GetNode(string node, List<string> nodes)
        {
            if (node == null || nodes == null) throw new ArgumentNullException("GetNode accepted null node or nodelist");

            foreach(string item in nodes)
            {
                if(node == item)
                {
                    return item;
                }
            }

            return null;
        }

        private string GetNodeWithLeastWeight()
        {
            double minValue = double.MaxValue;
            string node = null;

            foreach(var item in _nodeWeights)
            {
                if(item.Value < minValue && _traversed[item.Key] == false)
                {
                    minValue = item.Value;
                    node = item.Key;
                }
            }

            return node;
        }
    }
}
