﻿using System;
using System.Collections.Generic;
using Models;

namespace Source
{
    public class GraphParser
    {
        private readonly string[] _text;
        private readonly GraphData _graph;

        public GraphParser(string[] text)
        {
            _text = text ?? throw new ArgumentNullException(nameof(text));
            _graph = Parse();
        }

        public GraphData Parse()
        {
            GraphData graph = new GraphData();
            graph.Edges = new List<Edge>();
            graph.Nodes = new Dictionary<string, double[]>();

            foreach(string line in _text)
            {
                if (line.Contains("/link"))
                {
                    graph.Edges.Add(new ParseEdge(line).GetLink());
                }

                if (line.Contains("/node"))
                {
                    KeyValuePair<string, double[]> node = new ParseNode(line).GetNode();
                    graph.Nodes.Add(node.Key, node.Value);
                }
            }
            return graph;
        }

        public List<string> GetNodes()
        {
            List<string> nodes = new List<string>();
            foreach (var item in _graph.Nodes)
            {
                nodes.Add(item.Key);
            }

            return nodes;
        }
    }
}
