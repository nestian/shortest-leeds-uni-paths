﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class GraphData
    {
        public List<Edge> Edges { get; set; }
        public Dictionary<string, double[]> Nodes { get; set; }
    }
}
