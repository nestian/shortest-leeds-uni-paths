using System;
using System.IO;
using System.Collections.Generic;
using Source;
using Models;
using Xunit;
using FluentAssertions;

namespace ShortestPathTests
{
    public class PathTests
    {
        private readonly List<string> _nodes;
        private readonly List<Edge> _edges;
        private readonly PathFinder _pathFinder;
        private readonly GraphData _graph;

        public PathTests()
        {
            _nodes = InitNodes();
            _edges = InitEdges();
            _pathFinder = new PathFinder(_nodes, _edges);
            _graph = new GraphParser(GetText("mapdata.txt")).Parse();
        }

        private static string[] GetText(string fileName)
        {
            return File.ReadAllLines(fileName) ?? throw new ArgumentNullException(nameof(fileName));
        }

        private List<string> InitNodes()
        {
            return new List<string>
            {
                "a","b","c","d","e"
            };
        }

        private List<Edge> InitEdges()
        {
            return new List<Edge>
            {
                new Edge
                {
                    Id = "ab",
                    Nodes = new string[]
                    {
                        "a",
                        "b"
                    },
                    Length = 2.0
                },

                new Edge
                {
                    Id = "ac",
                    Nodes = new string[]
                    {
                        "a",
                        "c"
                    },
                    Length = 3.0
                },

                new Edge
                {
                    Id = "cd",
                    Nodes = new string[]
                    {
                        "c",
                        "d"
                    },
                    Length = 3.0
                },

                new Edge
                {
                    Id = "bd",
                    Nodes = new string[]
                    {
                        "b",
                        "d"
                    },
                    Length = 5.0
                }
            };
        }

        [Fact]
        public void TestAdjacentSets()
        {
            Dictionary<string, List<KeyValuePair<string, double>>> expected =
                new Dictionary<string, List<KeyValuePair<string, double>>>();

            List<KeyValuePair<string, double>> ofA = new List<KeyValuePair<string, double>>();
            List<KeyValuePair<string, double>> ofB = new List<KeyValuePair<string, double>>();
            List<KeyValuePair<string, double>> ofC = new List<KeyValuePair<string, double>>(); 
            List<KeyValuePair<string, double>> ofD = new List<KeyValuePair<string, double>>();
            List<KeyValuePair<string, double>> ofE = null;

            ofA.Add(new KeyValuePair<string, double>("b", 2.0));
            ofA.Add(new KeyValuePair<string, double>("c", 3.0));
            ofB.Add(new KeyValuePair<string, double>("a", 2.0));
            ofB.Add(new KeyValuePair<string, double>("d", 5.0));
            ofC.Add(new KeyValuePair<string, double>("a", 3.0));
            ofC.Add(new KeyValuePair<string, double>("d", 3.0));
            ofD.Add(new KeyValuePair<string, double>("c", 3.0));
            ofD.Add(new KeyValuePair<string, double>("b", 5.0));

            expected.Add("a", ofA);
            expected.Add("b", ofB);
            expected.Add("c", ofC);
            expected.Add("d", ofD);
            expected.Add("e", ofE);

            var actual = new AdjacentSets(_nodes, _edges).GetAll();

            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void TestPathFinder()
        {
            List<string> expectedAtoD = new List<string>
            {
                "a","c","d"
            };

            List<string> expectedDtoA = new List<string>
            {
                "d", "c", "a"
            };

            List<string> expectedCtoB = new List<string>
            {
                "c", "a", "b"
            };

            List<string> actualAtoD = _pathFinder.GetShortestPath("a", "d");
            List<string> actualDtoA = _pathFinder.GetShortestPath("d", "a");
            List<string> actualCtoB = _pathFinder.GetShortestPath("c", "b");
            List<string> actualEtoA = _pathFinder.GetShortestPath("e", "a");
            
            foreach(string item in expectedAtoD)
            {
                actualAtoD.Should().ContainEquivalentOf(item);
            }

            foreach (string item in expectedDtoA)
            {
                actualDtoA.Should().ContainEquivalentOf(item);
            }

            foreach (string item in expectedCtoB)
            {
                actualCtoB.Should().ContainEquivalentOf(item);
            }

            actualEtoA.Should().BeNull();

        }

        [Fact]
        public void TestParseFormatter()
        {
            string linkLine = @"<link id=-2143392622 node=-8847 node=-8849 way=-8850 length=11.006410 veg=0.000000 arch=0.000000 land=0.000000 POI=;/link>";
            string nodeLine = @"<node id=-8847 lat=53.807817 lon=-1.562377 /node>";
            List<KeyValuePair<string, string>> expected = new List<KeyValuePair<string, string>>();

            expected.Add(new KeyValuePair<string, string>("id", "-2143392622"));
            expected.Add(new KeyValuePair<string, string>("node", "-8847"));
            expected.Add(new KeyValuePair<string, string>("node", "-8849"));
            expected.Add(new KeyValuePair<string, string>("way", "-8850"));
            expected.Add(new KeyValuePair<string, string>("length", "11.006410"));
            expected.Add(new KeyValuePair<string, string>("veg", "0.000000"));
            expected.Add(new KeyValuePair<string, string>("arch", "0.000000"));
            expected.Add(new KeyValuePair<string, string>("land", "0.000000"));
            expected.Add(new KeyValuePair<string, string>("POI", ""));

            List<KeyValuePair<string, string>> actual = new ParseFormatter(linkLine, "link").GetAttributes();

            actual.Should().BeEquivalentTo(expected);

            expected.Clear();
            actual.Clear();

            expected.Add(new KeyValuePair<string, string>("id", "-8847"));
            expected.Add(new KeyValuePair<string, string>("lat", "53.807817"));
            expected.Add(new KeyValuePair<string, string>("lon", "-1.562377"));

            actual = new ParseFormatter(nodeLine, "node").GetAttributes();

            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void TestParseEdge()
        {
            Edge expectedEdge1 = new Edge();
            expectedEdge1.Id = "-2143392622";
            expectedEdge1.Length = 11.006410;
            expectedEdge1.Nodes = new string[2]
            {
                "-8847","-8849"
            };

            Edge expectedEdge2 = new Edge();
            expectedEdge2.Id = "-2143395348";
            expectedEdge2.Length = 32.710119;
            expectedEdge2.Nodes = new string[2]
            {
                "57620252","985096754"
            };

            _graph.Edges.Should().ContainEquivalentOf(expectedEdge1);
            _graph.Edges.Should().ContainEquivalentOf(expectedEdge2);

            _graph.Nodes["-8847"].Should().BeEquivalentTo(new double[2] { 53.807817, -1.562377 });
            _graph.Nodes["-1867901273"].Should().BeEquivalentTo(new double[2] { 53.801912, -1.563301 });
        }

        [Fact]
        public void TestPathFinderOnGraph()
        {
            List<string> nodes = new List<string>();
            foreach(var item in _graph.Nodes)
            {
                nodes.Add(item.Key);
            }

            PathFinder pathObj = new PathFinder(nodes, _graph.Edges);
            List<string> path1 = pathObj.GetShortestPath("26653396", "27081298");
        }
    }
}
